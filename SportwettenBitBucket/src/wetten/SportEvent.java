package wetten;

import java.util.HashMap;

public class SportEvent
{
	private Sportart sportart;
	private SportEvent sportevent;
	private HashMap<SportEvent, Sportart> event=new HashMap<SportEvent, Sportart>();
	private HashMap<SportEvent, Results> result=new HashMap<SportEvent, Results>();
	
	public SportEvent(Sportart art)
	{
		this.sportart=art;
		sportevent=this;
		event.put(sportevent, sportart);
		result.put(sportevent, new Results(sportart));
	}
	
	public HashMap<SportEvent, Sportart> getEvent()
	{
		return event;
	}
}
