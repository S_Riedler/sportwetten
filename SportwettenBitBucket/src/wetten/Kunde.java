package wetten;

import java.util.ArrayList;

public class Kunde
{
	private String name;
	private Wetten wette;
	private ArrayList<Wetten> wettenListe=new ArrayList<Wetten>();
	
	public Kunde(String name, Wetten wette)
	{
		this.name=name;
		this.wette=wette;
		wettenListe.add(wette);
	}
	
	@Override
	public String toString()
	{
		return name + " hat " + wettenListe.size() + " am laufen.";
	}
}
