package wetten;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SportGUI extends JFrame
{
	private static final long serialVersionUID = 1536098636950727885L;
	
	private GridBagConstraints gridBagC;
	
	private JPanel pnl_main_layout;
	private JPanel pnl_main_top;
	private JPanel pnl_main_left;
	private JPanel pnl_main_right;
	private JPanel pnl_main_bottom;
	private JPanel pnl_main_centre;
	private JPanel pnl_main_centre_top;
	private JPanel pnl_main_centre_centre;
	private JPanel pnl_main_centre_bottom;
	private JPanel pnl_main_centre_left;
	private JPanel pnl_main_centre_right;
	
	private JLabel lbl_results;
	
	private JTextArea txt_guest;
	private JLabel lbl_vs;
	private JTextArea txt_home;
	
	private JButton btn_close;
	
	private String heim="empty";
	private String guest="empty";
	
	private Font font_lbl;
	private Font font_btn;
	private Font font_home_textArea;
	private Font font_guest_textArea;
	private Font font_vs;
	
	public SportGUI()
	{
		setTitle("SportWetten - Die Gewinner gibt's hier!");
		setLocationRelativeTo(null);
		setSize(Toolkit.getDefaultToolkit().getScreenSize());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setDefaultLookAndFeelDecorated(true);
		
		initComponents();
		
		setContentPane(pnl_main_layout);
		
		setVisible(true);
	}
	
	public void initComponents()
	{
		this.getContentPane().setLayout(new BorderLayout());
		
		this.gridBagC=new GridBagConstraints();
		this.gridBagC.gridx=0;
		this.gridBagC.gridy=0;
		this.gridBagC.gridheight=20;
		this.gridBagC.gridwidth=100;
		
		this.pnl_main_layout=new JPanel(new BorderLayout());
		this.pnl_main_bottom=new JPanel();
		this.pnl_main_top=new JPanel();
		this.pnl_main_right=new JPanel();
		this.pnl_main_left=new JPanel();
		this.pnl_main_centre=new JPanel(new BorderLayout());
		this.pnl_main_centre_centre=new JPanel(new GridBagLayout());
		this.pnl_main_centre_bottom=new JPanel();
		this.pnl_main_centre_top=new JPanel(new GridBagLayout());
		this.pnl_main_centre_left=new JPanel();
		this.pnl_main_centre_right=new JPanel();
		
		this.pnl_main_bottom.setPreferredSize(new Dimension(getWidth(), 30));
		this.pnl_main_bottom.setBackground(Color.DARK_GRAY);
		this.pnl_main_top.setPreferredSize(new Dimension(500, 30));
		this.pnl_main_top.setBackground(Color.DARK_GRAY);
		this.pnl_main_right.setPreferredSize(new Dimension(30, getHeight()));
		this.pnl_main_right.setBackground(Color.DARK_GRAY);
		this.pnl_main_left.setPreferredSize(new Dimension(30, getHeight()));
		this.pnl_main_left.setBackground(Color.DARK_GRAY);
		
		this.pnl_main_centre.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		
		this.pnl_main_centre_centre.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		this.pnl_main_centre_centre.setBackground(Color.DARK_GRAY);
		this.pnl_main_centre_left.setPreferredSize(new Dimension(50, getHeight()));
		this.pnl_main_centre_left.setBackground(Color.LIGHT_GRAY.darker());
		this.pnl_main_centre_right.setPreferredSize(new Dimension(50, getHeight()));
		this.pnl_main_centre_right.setBackground(Color.LIGHT_GRAY.darker());
		this.pnl_main_centre_top.setPreferredSize(new Dimension(getWidth(), 50));
		this.pnl_main_centre_top.setBackground(Color.LIGHT_GRAY.darker());
		this.pnl_main_centre_bottom.setBackground(Color.LIGHT_GRAY.darker());
		
		this.txt_home=new JTextArea(this.heim);
		this.txt_guest=new JTextArea(this.guest);
		
		this.txt_home.setAutoscrolls(true);
		this.txt_home.setPreferredSize(new Dimension(300, 100));
		this.txt_home.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedSoftBevelBorder(), "Home"));
		this.txt_home.setSelectionColor(Color.BLACK.brighter());
		this.txt_home.setBackground(Color.LIGHT_GRAY);
		this.txt_home.setFont(font_home_textArea);
		
		this.txt_guest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedSoftBevelBorder(), "Guest"));
		this.txt_guest.setPreferredSize(new Dimension(300, 100));
		this.txt_guest.setSelectionColor(Color.BLACK.brighter());
		this.txt_guest.setBackground(Color.LIGHT_GRAY);
		this.txt_guest.setFont(font_guest_textArea);
		
		this.font_lbl=new Font(Font.SERIF, ((Font.BOLD)+(Font.ITALIC)), 20);
		this.font_btn=new Font(Font.SERIF, ((Font.BOLD)+(Font.ITALIC)), 15);
		this.font_home_textArea=new Font(Font.SERIF, ((Font.BOLD)), 15);
		this.font_guest_textArea=new Font(Font.SERIF, ((Font.BOLD)), 15);
		this.font_vs=new Font(Font.SERIF, ((Font.BOLD)+(Font.ITALIC)), 15);
		
		this.lbl_results=new JLabel("Results...");
		this.lbl_results.setVerticalTextPosition(JLabel.CENTER);
		this.lbl_results.setHorizontalTextPosition(JLabel.CENTER);
		this.lbl_results.setFont(font_lbl);
		this.lbl_results.setForeground(Color.BLACK.brighter());
		
		this.lbl_vs=new JLabel("  vs. ");
		this.lbl_vs.setVerticalTextPosition(JLabel.CENTER);
		this.lbl_vs.setHorizontalTextPosition(JLabel.CENTER);
		this.lbl_vs.setBorder(BorderFactory.createRaisedBevelBorder());
		this.lbl_vs.setPreferredSize(new Dimension(45, 35));
		this.lbl_vs.setFont(font_vs);
		this.lbl_vs.setForeground(Color.BLACK.darker());
		
		this.btn_close=new JButton("Loose (Close)");
		this.btn_close.setFont(font_btn);
		this.btn_close.setForeground(Color.BLACK.brighter());
		this.btn_close.setPreferredSize(new Dimension(200, 40));
		this.btn_close.setBorder(BorderFactory.createRaisedBevelBorder());
		this.btn_close.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
		
		this.pnl_main_centre_bottom.add(btn_close);
		this.pnl_main_centre_top.add(lbl_results);
		this.pnl_main_layout.add(pnl_main_centre, BorderLayout.CENTER);
		this.pnl_main_layout.add(pnl_main_bottom, BorderLayout.SOUTH);
		this.pnl_main_layout.add(pnl_main_right, BorderLayout.EAST);
		this.pnl_main_layout.add(pnl_main_left, BorderLayout.WEST);
		this.pnl_main_layout.add(pnl_main_top, BorderLayout.NORTH);
		this.pnl_main_centre.add(pnl_main_centre_centre, BorderLayout.CENTER);
		this.pnl_main_centre.add(pnl_main_centre_bottom, BorderLayout.SOUTH);
		this.pnl_main_centre.add(pnl_main_centre_top, BorderLayout.NORTH);
		this.pnl_main_centre.add(pnl_main_centre_left, BorderLayout.WEST);
		this.pnl_main_centre.add(pnl_main_centre_right, BorderLayout.EAST);
		this.pnl_main_centre_centre.add(txt_home);
		this.pnl_main_centre_centre.add(lbl_vs);
		this.pnl_main_centre_centre.add(txt_guest);
	}
	
	public static void main(String[] args)
	{
		new SportGUI();
	}
}
